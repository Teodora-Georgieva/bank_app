package com.example.frontend.utils;

import com.example.frontend.constants.Pages;

import javax.annotation.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import java.io.IOException;
import java.io.Serializable;

@ManagedBean
@ViewScoped
public class Router implements Serializable {
    public static void redirect(String path) throws IOException {
        FacesContext.getCurrentInstance().getExternalContext()
                .redirect(contextPath() + path);
    }

    private static String contextPath() {
        // empty for now
        return "";
    }

    public void goToRegister() throws IOException {
        redirect(Pages.REGISTER);
    }

    public static void goToLogin() throws IOException {
        redirect(Pages.LOGIN);
    }

    public void goToHomepage() throws IOException {
        redirect(Pages.HOMEPAGE);
    }

    public void goToDeposit() throws IOException {
        redirect(Pages.DEPOSIT);
    }

    public void goToWithdraw() throws IOException {
        redirect(Pages.WITHDRAW);
    }

    public void goToReport() throws IOException {
        redirect(Pages.REPORT);
    }
}