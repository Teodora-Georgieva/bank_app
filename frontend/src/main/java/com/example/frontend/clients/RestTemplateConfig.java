package com.example.frontend.clients;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {
    private final RestTemplateBuilder restTemplateBuilder;
    private final DummyErrorHandler dummyErrorHandler;
    public RestTemplateConfig(RestTemplateBuilder restTemplateBuilder,
                              DummyErrorHandler dummyErrorHandler) {
        this.restTemplateBuilder = restTemplateBuilder;
        this.dummyErrorHandler = dummyErrorHandler;
    }
    @Bean
    public RestTemplate httpClientRestTemplate() {
        return restTemplateBuilder.errorHandler(dummyErrorHandler).build();
    }
}