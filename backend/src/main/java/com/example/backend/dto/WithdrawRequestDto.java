package com.example.backend.dto;

import com.example.backend.validations.ValidWithdrawRequestDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ValidWithdrawRequestDto
public class WithdrawRequestDto {
    private Long bankAccountId;
    private BigDecimal withdrawAmount;
}