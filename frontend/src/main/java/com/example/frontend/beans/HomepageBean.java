package com.example.frontend.beans;

import com.example.frontend.clients.BankAccountClient;
import com.example.frontend.clients.TransactionClient;
import com.example.frontend.clients.UserClient;
import com.example.frontend.dto.*;
import com.example.frontend.http.HttpResponse;
import com.example.frontend.mapper.BankAccountMapper;
import com.example.frontend.mapper.TransactionMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Data
@ManagedBean
@ViewScoped
public class HomepageBean {
    @Autowired //there is a nullptr exception without it
    private BankAccountClient bankAccountClient;
    @Autowired
    private TransactionClient transactionClient;
    @Autowired
    private UserClient userClient;
    @Autowired
    private CurrentUserBean currentUserBean;
    private List<HomepageBankAccountDto> bankAccounts;
    private List<DisplayTransactionDto> transactions;

    @PostConstruct
    private void init() {
        HttpResponse bankAccountsResponse = bankAccountClient.getAllBankAccounts();
        BankAccounts bankAccountsBody = bankAccountsResponse.read(BankAccounts.class);
        bankAccounts = new ArrayList<>();
        for (BankAccountRespDto bankAccountRespDto : bankAccountsBody.getBankAccounts()) {
            HomepageBankAccountDto bankAccount = BankAccountMapper.homepageBankAccountFrom(bankAccountRespDto);
            bankAccounts.add(bankAccount);
        }

        HttpResponse transactionsResponse = transactionClient.getLastFiveTransactions();

        Transactions transactionsBody = transactionsResponse.read(Transactions.class);
        transactions = new ArrayList<>();
        for (TransactionRespDto transactionRespDto : transactionsBody.getUserTransactions()) {
            DisplayTransactionDto transaction = TransactionMapper.displayTransactionDtoFrom(transactionRespDto);
            transactions.add(transaction);
            if (transaction.getCurrentNumber() == transactionsBody.getUserTransactions().size()) {
                transaction.restoreCounterToOne();
            }
        }
    }
}