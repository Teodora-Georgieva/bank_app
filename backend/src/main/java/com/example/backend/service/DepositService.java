package com.example.backend.service;

import com.example.backend.dto.DepositRequestDto;
import com.example.backend.dto.TransactionRespDto;

public interface DepositService {
    TransactionRespDto deposit(DepositRequestDto requestDto);
}