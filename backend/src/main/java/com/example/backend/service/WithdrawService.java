package com.example.backend.service;

import com.example.backend.dto.TransactionRespDto;
import com.example.backend.dto.WithdrawRequestDto;

import java.math.BigDecimal;

public interface WithdrawService {
    BigDecimal findCurrentBalanceByBankAccountId(Long bankAccId);

    TransactionRespDto withdraw(WithdrawRequestDto requestDto);
}