package com.example.backend.validations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = WithdrawReqDtoValidator.class)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidWithdrawRequestDto {
    String message() default "Invalid withdraw request";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}