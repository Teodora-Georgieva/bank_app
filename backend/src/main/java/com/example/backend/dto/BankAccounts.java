package com.example.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class BankAccounts {
    private List<BankAccountRespDto> bankAccounts = new ArrayList<>();

    public void add(BankAccountRespDto bankAccountRespDto) {
        bankAccounts.add(bankAccountRespDto);
    }
}