package com.example.backend.utils;

public class NumberUtils {
    public static boolean isNumberInRange(int number, int min, int max) {
        return number >= min && number <= max;
    }
}