package com.example.frontend.beans;

import com.example.frontend.http.HttpResponse;
import com.example.frontend.views.MessageView;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.ManagedBean;
import javax.faces.view.ViewScoped;
import java.math.BigDecimal;

@Slf4j
@Data
@ManagedBean
@ViewScoped
public class DepositBean extends TransactionBean {
    private BigDecimal depositAmount;

    public void deposit() {
        HttpResponse resp = bankAccountClient.deposit(selectedBankAccountId, depositAmount);
        if (resp.isOK()) {
            MessageView.addSuccessMessage("Confirmed", "Deposit successfully made");
        } else {
            MessageView.addWarningMessage("Failure", "Deposit was rejected");
        }
    }

    public String addConfirmMessage() {
        return "Are you sure you want to make a deposit?";
    }
}