package com.example.backend.service;

import com.example.backend.currentuser.CurrentUser;
import com.example.backend.dto.DepositRequestDto;
import com.example.backend.dto.TransactionRespDto;
import com.example.backend.entity.BankAccountEntity;
import com.example.backend.entity.TransactionEntity;
import com.example.backend.entity.UserEntity;
import com.example.backend.enums.TransactionType;
import com.example.backend.mapper.TransactionMapper;
import com.example.backend.repository.TransactionRepository;
import com.example.backend.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;

@Service
@AllArgsConstructor
public class DepositServiceImpl implements DepositService {
    private final UserRepository userRepository;
    private final TransactionRepository transactionRepository;
    private final BankAccountService bankAccountService;
    private final CurrentUser currentUser;

    @Transactional
    public TransactionRespDto deposit(DepositRequestDto requestDto) {
        UserEntity userEntity = userRepository.findById(currentUser.getId()).get();
        BigDecimal depositAmount = requestDto.getDepositAmount();
        Long bankAccountId = requestDto.getBankAccountId();
        BankAccountEntity bankAccountEntity = bankAccountService.findById(bankAccountId);
        TransactionEntity deposit = new TransactionEntity();
        deposit.setUser(userEntity);
        deposit.setDepositOrWithdrawAmount(depositAmount);
        deposit.setTransactionType(TransactionType.DEPOSIT);
        deposit.setBankAccountEntity(bankAccountEntity);
        deposit = transactionRepository.save(deposit);
        bankAccountService.updateCurrentBalance(bankAccountId, depositAmount, true);
        TransactionRespDto depositResponseDto = TransactionMapper.transactionRespDtoFrom(deposit);
        return depositResponseDto;
    }
}