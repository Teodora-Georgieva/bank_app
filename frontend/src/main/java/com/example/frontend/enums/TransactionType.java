package com.example.frontend.enums;

public enum TransactionType {
    DEPOSIT, WITHDRAWAL
}