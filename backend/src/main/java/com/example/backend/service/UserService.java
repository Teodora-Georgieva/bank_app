package com.example.backend.service;

import com.example.backend.dto.RegistrationReqDto;
import com.example.backend.entity.UserEntity;

import java.util.Optional;

public interface UserService {
    UserEntity saveUser(RegistrationReqDto requestDto);

    UserEntity findById(Long userId);

    Optional<UserEntity> findByUsernameAndPassword(String username, String password);

    UserEntity findBySid(String sid);

    Integer getNumberOfOnlineUsers();
}