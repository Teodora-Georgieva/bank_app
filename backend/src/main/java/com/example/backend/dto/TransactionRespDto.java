package com.example.backend.dto;

import com.example.backend.enums.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionRespDto {
    private Long id;
    private BigDecimal transactionAmount;
    private LocalDateTime createdDate;
    private TransactionType transactionType;
    private String bankAccount;
}