package com.example.backend.controller;

import com.example.backend.constants.Endpoints;
import com.example.backend.dto.DepositRequestDto;
import com.example.backend.dto.TransactionRespDto;
import com.example.backend.service.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DepositController {
    @Autowired
    private DepositService depositService;

    @PostMapping(Endpoints.DEPOSIT)
    public ResponseEntity<?> deposit(@RequestBody DepositRequestDto requestDto) {
        TransactionRespDto transactionRespDto = depositService.deposit(requestDto);
        return ResponseEntity.ok(transactionRespDto);
    }
}