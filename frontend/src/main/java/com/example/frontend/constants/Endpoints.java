package com.example.frontend.constants;

public interface Endpoints {
    String GET_TOTAL_BALANCE = "/bankAccounts/totalBalance";
    String DEPOSIT = "/deposit";
    String GET_ALL_BANK_ACCOUNTS = "/bankAccounts/getAll";
    String WITHDRAW = "/withdraw";
    String GET_LAST_FIVE_TRANSACTIONS = "/transactions/lastFive";
    String GET_ALL_TRANSACTIONS_BETWEEN = "/transactions/getAllBetween/";
    String REGISTER = "/user/register";
    String LOGIN = "/user/login";
    String LOGOUT = "/user/logout";
    String GET_NUMBER_OF_ONLINE_USERS = "/getNumberOfOnlineUsers";
}