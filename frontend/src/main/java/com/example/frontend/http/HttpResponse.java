package com.example.frontend.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import java.io.IOException;

@Data
@NoArgsConstructor
@Slf4j
public class HttpResponse {
    private HttpStatus httpStatus;
    private String body;
    private ObjectMapper objectMapper = new ObjectMapper();

    public HttpResponse(HttpStatus httpStatus, String body) {
        this.httpStatus = httpStatus;
        this.body = body;
    }

    public <T> T read(Class<T> clazz) {
        try {
            objectMapper.registerModule(new JavaTimeModule());
            return objectMapper.readValue(this.body, clazz);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    public boolean isOK() {
        return httpStatus == HttpStatus.OK;
    }

    public boolean isNotFound() {
        return httpStatus == HttpStatus.NOT_FOUND;
    }

    public boolean isUnauthorized() {
        return httpStatus == HttpStatus.UNAUTHORIZED;
    }

    public boolean isBadRequest() {
        return httpStatus == HttpStatus.BAD_REQUEST;
    }
}