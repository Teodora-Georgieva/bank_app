package com.example.backend.controller;

import com.example.backend.constants.Endpoints;
import com.example.backend.dto.RegistrationReqDto;
import com.example.backend.dto.RegistrationRespDto;
import com.example.backend.service.UserRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class RegisterController {
    @Autowired
    private UserRegisterService userRegisterService;

    @PostMapping(Endpoints.REGISTER)
    public ResponseEntity<?> register(@Valid @RequestBody RegistrationReqDto requestDto) {
        RegistrationRespDto respDto = userRegisterService.register(requestDto);
        return ResponseEntity.ok(respDto);
    }
}