package com.example.frontend.clients;

import com.example.frontend.beans.CurrentUserBean;
import com.example.frontend.utils.Router;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.DefaultResponseErrorHandler;

import javax.faces.context.FacesContext;
import java.io.IOException;

@AllArgsConstructor
@Component
@Slf4j
public class DummyErrorHandler extends DefaultResponseErrorHandler {
    private final CurrentUserBean currentUserBean;

    @Override
    protected void handleError(ClientHttpResponse response, HttpStatus statusCode) throws IOException {
        // no actions as is dummy
        if (statusCode == HttpStatus.UNAUTHORIZED) {
            log.info("Could not login");
//            currentUserBean.setUserLoginRespDto(null);
//            Router.goToLogin();
//            FacesContext.getCurrentInstance().responseComplete();
//            FacesContext.getCurrentInstance().renderResponse();
        }
    }
}