package com.example.frontend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HomepageBankAccountDto {
    public static final String CURRENCY = "ILS";
    private String bankAccount;
    private BigDecimal currentBalance;

    public String getCurrency() {
        return this.CURRENCY;
    }
}