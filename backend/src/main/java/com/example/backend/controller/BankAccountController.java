package com.example.backend.controller;

import com.example.backend.constants.Endpoints;
import com.example.backend.dto.BankAccounts;
import com.example.backend.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class BankAccountController {
    @Autowired
    private BankAccountService bankAccountService;

    @GetMapping(Endpoints.GET_TOTAL_BALANCE)
    public ResponseEntity<?> getTotalBalance() {
        BigDecimal totalBalance = bankAccountService.getTotalBalance();
        return ResponseEntity.ok(totalBalance);
    }

    @GetMapping(Endpoints.GET_ALL_BANK_ACCOUNTS)
    public ResponseEntity<BankAccounts> getAllBankAccounts() {
        BankAccounts bankAccounts = bankAccountService.getAllBankAccounts();
        return ResponseEntity.ok(bankAccounts);
    }
}