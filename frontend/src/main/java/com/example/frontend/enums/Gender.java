package com.example.frontend.enums;

public enum Gender {
    MALE, FEMALE
}