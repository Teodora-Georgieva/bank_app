package com.example.backend.mapper;

import com.example.backend.dto.LoginRespDto;
import com.example.backend.dto.RegistrationReqDto;
import com.example.backend.dto.RegistrationRespDto;
import com.example.backend.entity.UserEntity;

public class UserMapper {
    public static UserEntity userEntityFrom(RegistrationReqDto reqDto) {
        UserEntity userEntity = new UserEntity();
        userEntity.setFirstName(reqDto.getFirstName());
        userEntity.setLastName(reqDto.getLastName());
        userEntity.setAge(reqDto.getAge());
        userEntity.setSocialId(reqDto.getSocialId());
        userEntity.setGender(reqDto.getGender());
        userEntity.setEmail(reqDto.getEmail());
        userEntity.setUsername(reqDto.getUsername());
        userEntity.setPassword(reqDto.getPassword());
        return userEntity;
    }

    public static RegistrationRespDto userRegistrationRespDtoFrom(UserEntity userEntity) {
        RegistrationRespDto respDto = new RegistrationRespDto();
        respDto.setId(userEntity.getId());
        respDto.setFirstName(userEntity.getFirstName());
        respDto.setLastName(userEntity.getLastName());
        respDto.setAge(userEntity.getAge());
        respDto.setSocialId(userEntity.getSocialId());
        respDto.setGender(userEntity.getGender());
        respDto.setEmail(userEntity.getEmail());
        respDto.setUsername(userEntity.getUsername());
        return respDto;
    }

    public static LoginRespDto userLoginRespDtoFrom(UserEntity userEntity) {
        LoginRespDto userLoginRespDto = new LoginRespDto();
        userLoginRespDto.setId(userEntity.getId());
        userLoginRespDto.setFirstName(userEntity.getFirstName());
        userLoginRespDto.setLastName(userEntity.getLastName());
        userLoginRespDto.setEmail(userEntity.getEmail());
        return userLoginRespDto;
    }
}