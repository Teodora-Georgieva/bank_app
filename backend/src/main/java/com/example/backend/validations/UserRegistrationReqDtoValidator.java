package com.example.backend.validations;

import com.example.backend.dto.RegistrationReqDto;
import com.example.backend.enums.Gender;
import com.example.backend.repository.UserRepository;
import com.example.backend.utils.NumberUtils;
import com.example.backend.utils.RegexUtils;
import com.example.backend.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UserRegistrationReqDtoValidator implements
        ConstraintValidator<ValidUserRegistrationReqDto, RegistrationReqDto> {
    private static final int NAMES_MIN_LENGTH = 2;
    private static final int MIN_AGE = 18;
    private static final int MAX_AGE = 110;
    private static final int MIN_PASSWORD_LENGTH = 3;
    private static final int MAX_PASSWORD_LENGTH = 30;
    private static final String LABEL_FIRST_NAME = "firstName";
    private static final String LABEL_LAST_NAME = "lastName";
    private static final String LABEL_USERNAME = "username";
    private static final int SOCIAL_ID_LENGTH = 10;
    private static final int MIN_BANK_NUMBER = 10;
    private static final int MAX_BANK_NUMBER = 99;
    private static final int MIN_BANK_BRANCH_NUMBER = 100;
    private static final int MAX_BANK_BRANCH_NUMBER = 999;
    private static final int MIN_BANK_ACCOUNT_NUMBER = 100000000;
    private static final int MAX_BANK_ACCOUNT_NUMBER = 999999999;
    private static final String BANK_NUMBER_LBL = "bankNumber";
    private static final String BANK_BRANCH_NUMBER_LBL = "bankBranchNumber";
    private static final String BANK_ACCOUNT_NUMBER_LBL = "bankAccountNumber";

    @Autowired
    private UserRepository userRepository;

    @Override
    public void initialize(ValidUserRegistrationReqDto constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(RegistrationReqDto requestDto, ConstraintValidatorContext context) {
        boolean valid = true;

        if (!isValidName(requestDto.getFirstName(), context, LABEL_FIRST_NAME)) {
            valid = false;
        }

        if (!isValidName(requestDto.getLastName(), context, LABEL_LAST_NAME)) {
            valid = false;
        }

        if (!isValidAge(requestDto.getAge(), context)) {
            valid = false;
        }

        if (!isValidSocialId(requestDto.getSocialId(), context)) {
            valid = false;
        }

        if (!isValidGender(requestDto.getGender(), context)) {
            valid = false;
        }

        if (!isValidEmail(requestDto.getEmail(), context)) {
            valid = false;
        }

        if (!isValidBankNumber(requestDto.getBankNumber(), context)) {
            valid = false;
        }

        if (!isValidBankBranchNumber(requestDto.getBankBranchNumber(), context)) {
            valid = false;
        }

        if (!isValidBankAccountNumber(requestDto.getBankAccountNumber(), context)) {
            valid = false;
        }

        if (!isValidName(requestDto.getUsername(), context, LABEL_USERNAME)) {
            valid = false;
        }

        if (userRepository.findByUsername(requestDto.getUsername()) != null) {
            context.buildConstraintViolationWithTemplate("username already exists")
                    .addNode("username").addConstraintViolation();
            valid = false;
        }

        if (!isValidPassword(requestDto.getPassword(), context)) {
            valid = false;
        }

        if (!isValidRepeatedPassword(requestDto.getPassword(),
                requestDto.getRepeatedPassword(), context)) {
            valid = false;
        }
        return valid;
    }

    private boolean isValidName(String name, ConstraintValidatorContext context, String label) {
        if (StringUtils.isNullOrEmptyString(name)) {
            context.buildConstraintViolationWithTemplate(label + " is null or empty")
                    .addNode(label).addConstraintViolation();
            return false;
        }

        if (StringUtils.isNotEmptyString(name) &&
                StringUtils.isStringShorterThan(name, NAMES_MIN_LENGTH)) {
            context.buildConstraintViolationWithTemplate(label + " is shorter than 2 symbols").
                    addNode(label).addConstraintViolation();
            return false;
        }
        return true;
    }

    private boolean isValidAge(int age, ConstraintValidatorContext context) {
        if (age >= MIN_AGE && age <= MAX_AGE) {
            return true;
        }

        context.buildConstraintViolationWithTemplate("invalid age").
                addNode("age").addConstraintViolation();
        return false;
    }

    private boolean isValidSocialId(String socialId, ConstraintValidatorContext context) {
        if (StringUtils.isNullOrEmptyString(socialId)) {
            context.buildConstraintViolationWithTemplate("socialId is null")
                    .addNode("socialId").addConstraintViolation();
            return false;
        }

        if (StringUtils.isNotEmptyString(socialId) &&
                socialId.length() != SOCIAL_ID_LENGTH) {
            context.buildConstraintViolationWithTemplate("socialId is not 10 symbols").
                    addNode("socialId").addConstraintViolation();
            return false;
        }

        if (!StringUtils.containsOnlyDigits(socialId)) {
            context.buildConstraintViolationWithTemplate("socialId doesn't contain only digits").
                    addNode("socialId").addConstraintViolation();
            return false;
        }
        return true;
    }

    private boolean isValidGender(Gender gender, ConstraintValidatorContext context) {
        if (gender == null) {
            context.buildConstraintViolationWithTemplate("gender is null").
                    addNode("gender").addConstraintViolation();
            return false;
        }
        return true;
    }

    private boolean isValidEmail(String email, ConstraintValidatorContext context) {
        if (StringUtils.isNullOrEmptyString(email)) {
            context.buildConstraintViolationWithTemplate("email is null or empty")
                    .addNode("email").addConstraintViolation();
            return false;
        }

        if (StringUtils.isNotEmptyString(email) && !RegexUtils.matchesRegex(email)) {
            context.buildConstraintViolationWithTemplate("email is not valid")
                    .addNode("email").addConstraintViolation();
            return false;
        }

        if (userRepository.findByEmail(email) != null) {
            context.buildConstraintViolationWithTemplate("email already exists")
                    .addNode("email").addConstraintViolation();
            return false;
            //todo throw exception?
        }
        return true;
    }

    private boolean isValidBankNumber(Integer bankNumber, ConstraintValidatorContext context) {
        if (bankNumber == null) {
            context.buildConstraintViolationWithTemplate(BANK_NUMBER_LBL + " is null or empty")
                    .addNode(BANK_NUMBER_LBL).addConstraintViolation();
            return false;
        }

        if (bankNumber != null &&
                !NumberUtils.isNumberInRange(bankNumber, MIN_BANK_NUMBER, MAX_BANK_NUMBER)) {
            context.buildConstraintViolationWithTemplate(BANK_NUMBER_LBL + " is invalid")
                    .addNode(BANK_NUMBER_LBL).addConstraintViolation();
            return false;
        }
        return true;
    }

    private boolean isValidBankBranchNumber(Integer bankBranchNumber, ConstraintValidatorContext context) {
        if (bankBranchNumber == null) {
            context.buildConstraintViolationWithTemplate(BANK_BRANCH_NUMBER_LBL + " is null or empty")
                    .addNode(BANK_BRANCH_NUMBER_LBL).addConstraintViolation();
            return false;
        }

        if (bankBranchNumber != null &&
                !NumberUtils.isNumberInRange(bankBranchNumber, MIN_BANK_BRANCH_NUMBER, MAX_BANK_BRANCH_NUMBER)) {
            context.buildConstraintViolationWithTemplate(BANK_BRANCH_NUMBER_LBL + " is invalid")
                    .addNode(BANK_BRANCH_NUMBER_LBL).addConstraintViolation();
            return false;
        }
        return true;
    }

    private boolean isValidBankAccountNumber(Integer bankAccountNumber, ConstraintValidatorContext context) {
        if (bankAccountNumber == null) {
            context.buildConstraintViolationWithTemplate(BANK_ACCOUNT_NUMBER_LBL + " is null or empty")
                    .addNode(BANK_ACCOUNT_NUMBER_LBL).addConstraintViolation();
            return false;
        }

        if (bankAccountNumber != null &&
                !NumberUtils.isNumberInRange(bankAccountNumber, MIN_BANK_ACCOUNT_NUMBER, MAX_BANK_ACCOUNT_NUMBER)) {
            context.buildConstraintViolationWithTemplate(BANK_NUMBER_LBL + " is invalid")
                    .addNode(BANK_ACCOUNT_NUMBER_LBL).addConstraintViolation();
            return false;
        }
        return true;
    }

    private boolean isValidPassword(String password, ConstraintValidatorContext context) {
        if (StringUtils.isNullOrEmptyString(password)) {
            context.buildConstraintViolationWithTemplate("password is null or empty").
                    addNode("password").addConstraintViolation();
            return false;
        }

        if (StringUtils.isNotEmptyString(password) &&
                !isStrongEnough(password)) {
            context.buildConstraintViolationWithTemplate("invalid password").
                    addNode("password").addConstraintViolation();
            return false;
        }
        return true;
        //todo stronger validation with regex
    }

    private boolean isStrongEnough(String password) {
        if (!StringUtils.matchesLengthRange(password, MIN_PASSWORD_LENGTH, MAX_PASSWORD_LENGTH) ||
                StringUtils.containsOnlySpaces(password)) {
            return false;
        }
        return true;
    }

    private boolean isValidRepeatedPassword(String initialPassword, String repeatedPassword, ConstraintValidatorContext context) {
        if (!initialPassword.equals(repeatedPassword)) {
            context.buildConstraintViolationWithTemplate("invalid repeated password").
                    addNode("repeatedPassword").addConstraintViolation();
            return false;
        }
        return true;
    }
}