package com.example.frontend.clients;

import com.example.frontend.constants.Endpoints;
import com.example.frontend.dto.LoginReqDto;
import com.example.frontend.dto.RegistrationReqDto;
import com.example.frontend.http.HttpRequest;
import com.example.frontend.http.HttpResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@AllArgsConstructor
@Component
public class UserClient {
    private final HttpRequest httpRequest;

    public HttpResponse registerUser(RegistrationReqDto request) {
        return httpRequest.post(Endpoints.REGISTER, request);
    }

    public HttpResponse loginUser(LoginReqDto request) {
        return httpRequest.put(Endpoints.LOGIN, request);
    }

    public void logoutUser() {
        httpRequest.post(Endpoints.LOGOUT, null);
    }

    public HttpResponse getNumberOfOnlineUsers() {
        return httpRequest.get(Endpoints.GET_NUMBER_OF_ONLINE_USERS);
    }
}