package com.example.backend.service;

import com.example.backend.dto.RegistrationReqDto;
import com.example.backend.entity.UserEntity;
import com.example.backend.mapper.UserMapper;
import com.example.backend.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@AllArgsConstructor
@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserEntity saveUser(RegistrationReqDto requestDto) {
        UserEntity userEntity = UserMapper.userEntityFrom(requestDto);
        userEntity = userRepository.save(userEntity);
        return userEntity;
    }

    public UserEntity findById(Long userId) {
        Optional<UserEntity> userEntityOptional = userRepository.findById(userId);
        if (userEntityOptional.isPresent()) {
            return userEntityOptional.get();
        }
        return null; //todo throw exception
    }

    public Optional<UserEntity> findByUsernameAndPassword(String username, String password) {
        Optional<UserEntity> userEntityOptional =
                userRepository.findByUsernameAndPassword(username, password);
        return userEntityOptional;
    }

    public UserEntity findBySid(String sid) {
        return userRepository.findBySid(sid);
    }

    @Override
    public Integer getNumberOfOnlineUsers() {
        return userRepository.getNumberOfOnlineUsers();
    }
}