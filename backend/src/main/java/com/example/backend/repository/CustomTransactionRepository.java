package com.example.backend.repository;

import com.example.backend.entity.TransactionEntity;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface CustomTransactionRepository {
    List<TransactionEntity> getAllTransactionsForUserAndPeriod(Long userId, LocalDate startDate, LocalDate endDate);
}