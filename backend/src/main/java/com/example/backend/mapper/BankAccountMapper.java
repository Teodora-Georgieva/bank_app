package com.example.backend.mapper;

import com.example.backend.dto.BankAccountRespDto;
import com.example.backend.dto.RegistrationReqDto;
import com.example.backend.entity.BankAccountEntity;

public class BankAccountMapper {
    public static BankAccountEntity bankAccountEntityFrom(RegistrationReqDto reqDto) {
        BankAccountEntity bankAccountEntity = new BankAccountEntity();
        bankAccountEntity.setBankNumber(reqDto.getBankNumber());
        bankAccountEntity.setBankBranchNumber(reqDto.getBankBranchNumber());
        bankAccountEntity.setBankAccountNumber(reqDto.getBankAccountNumber());
        return bankAccountEntity;
    }

    public static BankAccountRespDto bankAccountRespDtoFrom(BankAccountEntity bankAccountEntity) {
        BankAccountRespDto bankAccountRespDto = new BankAccountRespDto();
        bankAccountRespDto.setId(bankAccountEntity.getId());
        bankAccountRespDto.setBankNumber(bankAccountEntity.getBankNumber());
        bankAccountRespDto.setBankBranchNumber(bankAccountEntity.getBankBranchNumber());
        bankAccountRespDto.setBankAccountNumber(bankAccountEntity.getBankAccountNumber());
        bankAccountRespDto.setCurrentBalance(bankAccountEntity.getCurrentBalance());
        return bankAccountRespDto;
    }
}