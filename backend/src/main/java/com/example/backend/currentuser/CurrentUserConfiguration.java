package com.example.backend.currentuser;

import com.example.backend.entity.UserEntity;
import com.example.backend.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Configuration
@AllArgsConstructor
public class CurrentUserConfiguration {
    private final UserService userService;

    @Bean
    @RequestScope
    public CurrentUser get() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes instanceof ServletRequestAttributes) {
            HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
            String authHeader = request.getHeader("Authorization");
            String sid = authHeader.substring("Bearer".length()).trim();
            UserEntity user = userService.findBySid(sid);
            return CurrentUser.aNewCurrentUser()
                    .id(user.getId()).firstName(user.getFirstName()).lastName(user.getLastName())
                    .email(user.getEmail()).build();
        }
        return null;
    }
}