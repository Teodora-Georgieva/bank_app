package com.example.backend.utils;

public class StringUtils {
    public static boolean isNullOrEmptyString(String string) {
        return string == null || string.isEmpty();
    }

    public static boolean isNotEmptyString(String string) {
        return !isNullOrEmptyString(string);
    }

    public static boolean isStringShorterThan(String string, int length) {
        return string.length() < length;
    }

    public static boolean containsOnlyDigits(String socialId) {
        for (int i = 0; i < socialId.length(); i++) {
            char crrSymbol = socialId.charAt(i);
            if (!isDigit(crrSymbol)) {
                return false;
            }
        }
        return true;
    }

    private static boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    public static boolean containsOnlySpaces(String string) {
        return string.trim().length() == 0;
    }

    public static boolean matchesLengthRange(String string, int minLength, int maxLength) {
        int stringLength = string.length();
        return stringLength >= minLength && stringLength <= maxLength;
    }
}