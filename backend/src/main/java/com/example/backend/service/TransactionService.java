package com.example.backend.service;

import com.example.backend.dto.Transactions;

import java.time.LocalDate;

public interface TransactionService {
    Transactions getLastFiveTransactions();

    Transactions getAllTransactionsBetween(LocalDate startDate, LocalDate endDate);
}