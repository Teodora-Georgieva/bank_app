package com.example.backend.validations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UserRegistrationReqDtoValidator.class)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidUserRegistrationReqDto {
    String message() default "Invalid register request";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}