package com.example.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class LoginRespDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String sessionId;
}