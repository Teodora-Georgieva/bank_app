package com.example.frontend.enums;

public enum FilterOptions {
    TODAY("Today"),
    THIS_WEEK("This Week"),
    THIS_MONTH("This Month"),
    PERIOD("Period");

    private final String label;

    FilterOptions(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}