package com.example.frontend.http;

import com.example.frontend.beans.CurrentUserBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Component
@Slf4j
public class HttpRequest {
    private final RestTemplate restTemplate;
    private final CurrentUserBean currentUserBean;
    private static final String HOST = "http://localhost:6000";

    public HttpRequest(@Qualifier("httpClientRestTemplate") RestTemplate restTemplate, CurrentUserBean currentUserBean) {
        this.restTemplate = restTemplate;
        this.currentUserBean = currentUserBean;
    }

    public HttpResponse get(String path) {
        HttpHeaders headers = getHttpHeaders();
        HttpEntity<?> reqEntity = new HttpEntity<>(headers);
        String url = HOST + path;
        log.debug(url);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, reqEntity, String.class);
        return new HttpResponse(response.getStatusCode(), response.getBody());
    }

    public HttpResponse post(String path, Object object) {
        HttpHeaders headers = getHttpHeaders();
        HttpEntity<?> reqEntity = new HttpEntity<>(object, headers);
        String url = HOST + path;
        log.debug(url);
        ResponseEntity<String> response = restTemplate.postForEntity(url, reqEntity, String.class);
        return new HttpResponse(response.getStatusCode(), response.getBody());
    }

    public HttpResponse put(String path, Object object) {
        HttpHeaders headers = getHttpHeaders();
        HttpEntity<?> reqEntity = new HttpEntity<>(object, headers);
        String url = HOST + path;
        log.debug(url);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, reqEntity, String.class);
        return new HttpResponse(response.getStatusCode(), response.getBody());
    }

    private HttpHeaders getHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON); //it will send json
        if (currentUserBean.getUserLoginRespDto() != null) {
            headers.add("Authorization", "Bearer " + currentUserBean.getUserLoginRespDto().getSessionId());
        }
        return headers;
    }
}