package com.example.frontend.clients;

import com.example.frontend.constants.Endpoints;
import com.example.frontend.http.HttpRequest;
import com.example.frontend.http.HttpResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Slf4j
@Component
@AllArgsConstructor
public class TransactionClient {
    private final HttpRequest httpRequest;

    public HttpResponse getLastFiveTransactions() {
        return httpRequest.get(Endpoints.GET_LAST_FIVE_TRANSACTIONS);
    }

    public HttpResponse getAllTransactionsBetween(LocalDate startDate, LocalDate endDate) {
        return httpRequest.get(Endpoints.GET_ALL_TRANSACTIONS_BETWEEN + startDate + "/" + endDate);
    }
}