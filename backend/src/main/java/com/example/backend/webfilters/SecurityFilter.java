package com.example.backend.webfilters;

import com.example.backend.service.SessionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@AllArgsConstructor
@Component
@Slf4j
public class SecurityFilter implements Filter {
    private final SessionService sessionService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        String requestURI = req.getRequestURI();
        if (requestURI.equals("/user/login") || requestURI.equals("/user/register")) {
            chain.doFilter(request, response);
            log.debug("====");
            return;
        }

        String authorization = req.getHeader("Authorization");

        if (authorization == null) {
            log.debug("Header is missing");
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        if (!authorization.startsWith("Bearer")) {
            log.debug("Invalid header");
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        String sid = authorization.substring("Bearer".length()).trim();
        log.debug("sid: {}", sid);
        if (!sessionService.isValidSid(sid)) {
            log.debug("Sid not found or expired");
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        } else {
            sessionService.refresh(sid);
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}