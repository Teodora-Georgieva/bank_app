# CREATE DATABASE bank;
CREATE TABLE IF NOT EXISTS users
(
    id                  BIGINT AUTO_INCREMENT PRIMARY KEY,
    first_name          VARCHAR(50)             NOT NULL,
    last_name           VARCHAR(50)             NOT NULL,
    age                 INT                     NOT NULL,
    social_id           VARCHAR(10)             NOT NULL,
    gender              ENUM ('MALE', 'FEMALE') NOT NULL,
    email               VARCHAR(100)            NOT NULL,
    bank_number         VARCHAR(2)              NOT NULL,
    bank_branch_number  VARCHAR(3)              NOT NULL,
    bank_account_number VARCHAR(9)              NOT NULL,
    password            VARCHAR(100)            NOT NULL,
    username            VARCHAR(50)             NOT NULL
);

ALTER TABLE users
    ADD COLUMN created_date DATETIME NOT NULL;

ALTER TABLE users
    ADD COLUMN updated_date DATETIME NULL;

ALTER TABLE users
    DROP COLUMN bank_number,
    DROP COLUMN bank_branch_number,
    DROP COLUMN bank_account_number;

INSERT INTO users(first_name, last_name, age, social_id, gender, email, password, username, created_date, updated_date)
VALUES ('Ivan', 'Ivanov', 20, '2222222222', 'MALE', 'ivan@abv.bg', '123', 'ivan', '2021-06-15 09:51:00',
        '2021-06-15 09:51:00');

CREATE TABLE IF NOT EXISTS bank_accounts
(
    id                  BIGINT AUTO_INCREMENT        NOT NULL PRIMARY KEY,
    user_id             BIGINT,
    bank_number         VARCHAR(2)                   NOT NULL,
    bank_branch_number  VARCHAR(3)                   NOT NULL,
    bank_account_number VARCHAR(9)                   NOT NULL,
    amount              DECIMAL(13, 2)               NOT NULL,
    type                ENUM ('WITHDRAW', 'DEPOSIT') NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users (id)
);

ALTER TABLE bank_accounts
    ADD COLUMN created_date DATETIME NOT NULL;

ALTER TABLE bank_accounts
    ADD COLUMN updated_date DATETIME NULL;

ALTER TABLE bank_accounts
    ADD UNIQUE KEY unique_bank_data (bank_number, bank_branch_number, bank_account_number);

INSERT INTO bank_accounts(user_id, bank_number, bank_branch_number, bank_account_number, amount, created_date,
                          updated_date)
SELECT (SELECT id from users where social_id = '2222222222'),
       22,
       222,
       222222222,
       0,
       '2021-06-15 09:51:01',
       '2021-06-15 09:51:01';

CREATE TABLE transactions
(
    id              BIGINT AUTO_INCREMENT PRIMARY KEY,
    user_id         BIGINT                         NOT NULL,
    amount          DECIMAL(13, 4) DEFAULT 0,
    created_date    DATETIME                       NOT NULL,
    type            ENUM ('DEPOSIT', 'WITHDRAWAL') NOT NULL,
    bank_account_id BIGINT                         NOT NULL,
    CONSTRAINT transactions_bank_accounts_fk
        FOREIGN KEY (bank_account_id) REFERENCES bank_accounts (id),
    CONSTRAINT users_transactions_fk
        FOREIGN KEY (user_id) REFERENCES users (id)
);

INSERT INTO bank_accounts(user_id, bank_number, bank_branch_number, bank_account_number, amount,
                          bank.bank_accounts.created_date, bank.bank_accounts.updated_date)
SELECT ((SELECT id FROM users WHERE social_id = '2222222222')),
       11,
       111,
       111111111,
       0,
       '2021-06-14 11:40:20',
       '2021-06-14 11:40:20';

INSERT INTO bank_accounts(user_id, bank_number, bank_branch_number, bank_account_number, amount,
                          bank.bank_accounts.created_date, bank.bank_accounts.updated_date)
SELECT (SELECT id FROM users WHERE social_id = '2222222222'),
       55,
       555,
       555555555,
       1,
       '2021-06-16 13:19:00',
       '2021-06-16 13:19:00';

ALTER TABLE bank_accounts
    MODIFY COLUMN amount DECIMAL(13, 4) NOT NULL;

CREATE TABLE user_session
(
    id         BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    user_id    BIGINT(20)  NOT NULL,
    sid        VARCHAR(50) NOT NULL,
    expires_on DATETIME    NOT NULL
);