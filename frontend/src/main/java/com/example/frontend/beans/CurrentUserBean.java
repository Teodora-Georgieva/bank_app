package com.example.frontend.beans;

import com.example.frontend.clients.UserClient;
import com.example.frontend.dto.LoginRespDto;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.annotation.SessionScope;

import javax.annotation.ManagedBean;

@Data
@ManagedBean
@SessionScope
public class CurrentUserBean {
    @Autowired
    private UserClient userClient;
    private LoginRespDto userLoginRespDto;

    public Integer getNumberOfOnlineUsers() {
        return userClient.getNumberOfOnlineUsers().read(Integer.class);
    }
}