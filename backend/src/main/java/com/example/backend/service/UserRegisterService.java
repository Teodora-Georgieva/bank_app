package com.example.backend.service;

import com.example.backend.dto.RegistrationReqDto;
import com.example.backend.dto.RegistrationRespDto;

public interface UserRegisterService {
    RegistrationRespDto register(RegistrationReqDto requestDto);
}