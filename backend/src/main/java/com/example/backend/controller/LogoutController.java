package com.example.backend.controller;

import com.example.backend.constants.Endpoints;
import com.example.backend.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LogoutController {
    @Autowired
    private SessionService sessionService;

    @PostMapping(Endpoints.LOGOUT)
    public void logout() {
        sessionService.logout();
    }
}