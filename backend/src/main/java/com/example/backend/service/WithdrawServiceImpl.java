package com.example.backend.service;

import com.example.backend.currentuser.CurrentUser;
import com.example.backend.dto.TransactionRespDto;
import com.example.backend.dto.WithdrawRequestDto;
import com.example.backend.entity.BankAccountEntity;
import com.example.backend.entity.TransactionEntity;
import com.example.backend.entity.UserEntity;
import com.example.backend.enums.TransactionType;
import com.example.backend.mapper.TransactionMapper;
import com.example.backend.repository.TransactionRepository;
import com.example.backend.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;

@Service
@AllArgsConstructor
public class WithdrawServiceImpl implements WithdrawService {
    private final UserRepository userRepository;
    private final TransactionRepository transactionRepository;
    private final BankAccountService bankAccountService;
    private final CurrentUser currentUser;

    public BigDecimal findCurrentBalanceByBankAccountId(Long bankAccId) {
        BankAccountEntity bankAccountEntity = bankAccountService.findById(bankAccId);
        return bankAccountEntity.getCurrentBalance();
    }

    @Transactional
    public TransactionRespDto withdraw(WithdrawRequestDto requestDto) {
        UserEntity userEntity = userRepository.findById(currentUser.getId()).get();
        Long bankAccountId = requestDto.getBankAccountId();
        BankAccountEntity bankAccountEntity = bankAccountService.findById(bankAccountId);
        BigDecimal withdrawAmount = requestDto.getWithdrawAmount();
        TransactionEntity withdraw = new TransactionEntity();
        withdraw.setUser(userEntity);
        withdraw.setDepositOrWithdrawAmount(withdrawAmount);
        withdraw.setTransactionType(TransactionType.WITHDRAWAL);
        withdraw.setBankAccountEntity(bankAccountEntity);
        withdraw = transactionRepository.save(withdraw);
        bankAccountService.updateCurrentBalance(bankAccountId, withdrawAmount, false);
        TransactionRespDto withdrawRespDto = TransactionMapper.transactionRespDtoFrom(withdraw);
        return withdrawRespDto;
    }
}