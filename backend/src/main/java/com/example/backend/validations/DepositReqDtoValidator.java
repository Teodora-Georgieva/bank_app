package com.example.backend.validations;

import com.example.backend.dto.DepositRequestDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

public class DepositReqDtoValidator implements
        ConstraintValidator<ValidDepositRequestDto, DepositRequestDto> {

    @Override
    public void initialize(ValidDepositRequestDto constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(DepositRequestDto requestDto, ConstraintValidatorContext context) {
        BigDecimal depositAmount = requestDto.getDepositAmount();
        if (depositAmount.compareTo(BigDecimal.ZERO) < 0) {
            context.
                    buildConstraintViolationWithTemplate("cannot deposit - deposit amount is less than 0").
                    addNode("depositAmount").addConstraintViolation();
            return false;
        }
        return true;
    }
}