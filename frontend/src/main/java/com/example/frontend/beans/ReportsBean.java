package com.example.frontend.beans;

import com.example.frontend.clients.TransactionClient;
import com.example.frontend.dto.DisplayTransactionDto;
import com.example.frontend.dto.TransactionRespDto;
import com.example.frontend.dto.Transactions;
import com.example.frontend.enums.FilterOptions;
import com.example.frontend.http.HttpResponse;
import com.example.frontend.mapper.TransactionMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
@Slf4j
@Data
public class ReportsBean {
    @Autowired
    private TransactionClient transactionClient;
    @Autowired
    private CurrentUserBean currentUserBean;

    private FilterOptions selectedFilterOption;
    private List<LocalDate> period = new ArrayList<>();
    private List<DisplayTransactionDto> transactions;

    @PostConstruct
    private void init() {
        period.add(0, null);
        period.add(1, null);
        this.selectedFilterOption = FilterOptions.THIS_WEEK;
        showTransactionsList();
    }

    public void showTransactionsList() {
        calculateStartAndEndDate();
        LocalDate startDate = period.get(0);
        LocalDate endDate = period.get(1);
        showAllTransactionsBetween(startDate, endDate);
    }

    private void calculateStartAndEndDate() {
        if(selectedFilterOption != FilterOptions.PERIOD) {
            period.set(1, LocalDate.now());

            switch (selectedFilterOption) {
                case TODAY:
                    period.set(0, LocalDate.now());
                    break;
                case THIS_WEEK:
                    period.set(0, getStartDateOfWeek(period.get(1)));
                    break;
                case THIS_MONTH:
                    period.set(0, getStartDateOfMonth(period.get(1)));
                    break;
            }
        }
    }

    private LocalDate getStartDateOfWeek(LocalDate endDate) {
        return endDate.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
    }

    private LocalDate getStartDateOfMonth(LocalDate endDate) {
        return endDate.with(TemporalAdjusters.firstDayOfMonth());
    }

    private void showAllTransactionsBetween(LocalDate startDate, LocalDate endDate) {
        HttpResponse transactionsResponse = transactionClient.getAllTransactionsBetween(startDate, endDate);
        Transactions transactionsBody = transactionsResponse.read(Transactions.class);
        transactions = new ArrayList<>();
        for (TransactionRespDto transactionRespDto : transactionsBody.getUserTransactions()) {
            DisplayTransactionDto transaction = TransactionMapper.displayTransactionDtoFrom(transactionRespDto);
            transactions.add(transaction);
            if (transaction.getCurrentNumber() == transactionsBody.getUserTransactions().size()) {
                transaction.restoreCounterToOne();
            }
        }
    }

    public FilterOptions[] getFilterOptions() {
        return FilterOptions.values();
    }

    public boolean disableDatePicker() {
        if (selectedFilterOption != FilterOptions.PERIOD) {
            return true;
        }
        return false;
    }
}