package com.example.backend.dto;

import com.example.backend.validations.ValidDepositRequestDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ValidDepositRequestDto
public class DepositRequestDto {
    private Long bankAccountId;
    private BigDecimal depositAmount;
}