package com.example.backend.service;

import com.example.backend.currentuser.CurrentUser;
import com.example.backend.dto.BankAccountRespDto;
import com.example.backend.dto.BankAccounts;
import com.example.backend.dto.RegistrationReqDto;
import com.example.backend.entity.BankAccountEntity;
import com.example.backend.entity.UserEntity;
import com.example.backend.mapper.BankAccountMapper;
import com.example.backend.repository.BankAccountRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class BankAccountServiceImpl implements BankAccountService {
    private final BankAccountRepository bankAccountRepository;
    private final UserService userService;
    private final CurrentUser currentUser;

    public BankAccountEntity saveBankAccount(RegistrationReqDto requestDto, UserEntity userEntity) {
        BankAccountEntity bankAccountEntity = BankAccountMapper.bankAccountEntityFrom(requestDto);
        bankAccountEntity.setCurrentBalance(BigDecimal.ZERO);
        bankAccountEntity.setUser(userEntity);
        bankAccountEntity = bankAccountRepository.save(bankAccountEntity);
        return bankAccountEntity;
    }

    public BigDecimal getTotalBalance() {
        UserEntity userEntity = userService.findById(currentUser.getId());
        List<BankAccountEntity> bankAccounts = userEntity.getBankAccounts();
        BigDecimal totalBalance = BigDecimal.ZERO;
        for (BankAccountEntity bankAccount : bankAccounts) {
            totalBalance = totalBalance.add(bankAccount.getCurrentBalance());
        }

        return totalBalance;
    }

    public void updateCurrentBalance(Long bankAccountId, BigDecimal transactionAmount, boolean isDeposit) {
        Optional<BankAccountEntity> bankAccountEntityOptional = bankAccountRepository.findById(bankAccountId);
        BankAccountEntity bankAccountEntity = null;
        if (bankAccountEntityOptional.isPresent()) {
            bankAccountEntity = bankAccountEntityOptional.get();
        } else {
            //todo throw exception
        }

        BigDecimal currentBalance = bankAccountEntity.getCurrentBalance();
        if (isDeposit) {
            currentBalance = currentBalance.add(transactionAmount);
        } else {
            currentBalance = currentBalance.subtract(transactionAmount);
        }

        bankAccountEntity.setCurrentBalance(currentBalance);
        bankAccountRepository.save(bankAccountEntity);
    }

    public BankAccounts getAllBankAccounts() {
        UserEntity userEntity = userService.findById(currentUser.getId());
        BankAccounts bankAccounts = new BankAccounts();
        List<BankAccountEntity> list = userEntity.getBankAccounts();
        for (BankAccountEntity bankAccountEntity : list) {
            BankAccountRespDto bankAccountRespDto = BankAccountMapper.bankAccountRespDtoFrom(bankAccountEntity);
            bankAccounts.add(bankAccountRespDto);
        }
        return bankAccounts;
    }

    public BankAccountEntity findById(Long bankAccountId) {
        Optional<BankAccountEntity> bankAccountEntityOptional = bankAccountRepository.findById(bankAccountId);
        if (bankAccountEntityOptional.isPresent()) {
            return bankAccountEntityOptional.get();
        }

        //todo throw exception
        return null;
    }
}