package com.example.backend.utils;

import java.util.regex.Pattern;

public class RegexUtils {
    private static final String EMAIL_REGEX = "[\\w\\.-]*[a-zA-Z0-9_]@[\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]";

    public static boolean matchesRegex(String email) {
        Pattern pattern = Pattern.compile(EMAIL_REGEX);
        return pattern.matcher(email).matches();
    }
}