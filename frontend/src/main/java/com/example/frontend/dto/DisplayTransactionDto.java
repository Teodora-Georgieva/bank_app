package com.example.frontend.dto;

import com.example.frontend.enums.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.context.annotation.SessionScope;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SessionScope
public class DisplayTransactionDto {
    private static Long counter = 1L;
    private Long currentNumber = counter++;
    private String createdDate;
    private TransactionType transactionType;
    private String bankAccount;
    private BigDecimal transactionAmount;

    public void restoreCounterToOne() {
        counter = 1L;
    }

    public boolean isDeposit() {
        return transactionType == TransactionType.DEPOSIT;
    }

    public boolean isWithdrawal() {
        return transactionType == TransactionType.WITHDRAWAL;
    }
}