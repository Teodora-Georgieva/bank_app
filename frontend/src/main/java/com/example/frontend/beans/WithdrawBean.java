package com.example.frontend.beans;

import com.example.frontend.http.HttpResponse;
import com.example.frontend.views.MessageView;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.ManagedBean;
import javax.faces.view.ViewScoped;
import java.math.BigDecimal;

@Slf4j
@Data
@ManagedBean
@ViewScoped
public class WithdrawBean extends TransactionBean {
    private BigDecimal withdrawAmount;

    public void withdraw() {
        HttpResponse resp = bankAccountClient.withdraw(selectedBankAccountId, withdrawAmount);

        if (resp.isOK()) {
            MessageView.addSuccessMessage("Confirmed", "Withdraw successfully made");
        } else {
            MessageView.addWarningMessage("Failure", "Withdraw was rejected");
        }
    }

    public String addConfirmMessage() {
        return "Are you sure you want to make a withdrawal?";
    }
}