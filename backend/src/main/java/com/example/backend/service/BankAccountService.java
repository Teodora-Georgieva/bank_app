package com.example.backend.service;

import com.example.backend.dto.BankAccounts;
import com.example.backend.dto.RegistrationReqDto;
import com.example.backend.entity.BankAccountEntity;
import com.example.backend.entity.UserEntity;

import java.math.BigDecimal;

public interface BankAccountService {
    BankAccountEntity saveBankAccount(RegistrationReqDto requestDto, UserEntity userEntity);

    BigDecimal getTotalBalance();

    void updateCurrentBalance(Long bankAccountId, BigDecimal transactionAmount, boolean isDeposit);

    BankAccounts getAllBankAccounts();

    BankAccountEntity findById(Long bankAccountId);
}