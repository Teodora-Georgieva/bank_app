package com.example.frontend.dto;

import com.example.frontend.enums.Gender;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegistrationReqDto {
    private String firstName;
    private String lastName;
    private Integer age;
    private String socialId;
    private Gender gender;
    private String email;
    private Integer bankNumber;
    private Integer bankBranchNumber;
    private Integer bankAccountNumber;
    private String username;
    private String password;
    private String repeatedPassword;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }
}