package com.example.frontend.beans;

import com.example.frontend.clients.BankAccountClient;
import com.example.frontend.dto.BankAccountRespDto;
import com.example.frontend.dto.BankAccounts;
import com.example.frontend.http.HttpResponse;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Data
@ManagedBean
@ViewScoped
public abstract class TransactionBean {
    @Autowired
    private CurrentUserBean currentUserBean;
    @Autowired
    BankAccountClient bankAccountClient;
    Long userId;
    BigDecimal totalBalance;
    Long selectedBankAccountId;
    List<BankAccountRespDto> bankAccountOptions;

    @PostConstruct
    public void init() {
        bankAccountOptions = new ArrayList<>();
        userId = currentUserBean.getUserLoginRespDto().getId();
        HttpResponse response = bankAccountClient.getAllBankAccounts();
        List<BankAccountRespDto> userBankAccounts = response.read(BankAccounts.class).getBankAccounts();
        this.bankAccountOptions.addAll(userBankAccounts);
    }

    public BigDecimal getTotalBalance() {
        HttpResponse response = bankAccountClient.getTotalBalance();
        return response.read(BigDecimal.class);
    }
}