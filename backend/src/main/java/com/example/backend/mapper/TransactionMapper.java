package com.example.backend.mapper;

import com.example.backend.dto.TransactionRespDto;
import com.example.backend.entity.TransactionEntity;
import com.google.common.collect.Lists;

import java.util.List;

public class TransactionMapper {
    public static TransactionRespDto transactionRespDtoFrom(TransactionEntity transaction) {
        TransactionRespDto transactionRespDto = new TransactionRespDto();
        transactionRespDto.setId(transaction.getId());
        transactionRespDto.setTransactionAmount(transaction.getDepositOrWithdrawAmount());
        transactionRespDto.setCreatedDate(transaction.getCreatedDate());
        transactionRespDto.setTransactionType(transaction.getTransactionType());

        Integer bankNumber = transaction.getBankAccountEntity().getBankNumber();
        Integer bankBranchNumber = transaction.getBankAccountEntity().getBankBranchNumber();
        Integer bankAccountNumber = transaction.getBankAccountEntity().getBankAccountNumber();
        String bankAccount = bankNumber + "-" + bankBranchNumber + "-" + bankAccountNumber;
        transactionRespDto.setBankAccount(bankAccount);
        return transactionRespDto;
    }

    public static List<TransactionRespDto> transactionDtosFrom(List<TransactionEntity> entities) {
        List<TransactionRespDto> dtos = Lists.newArrayList();
        entities.forEach(e -> dtos.add(transactionRespDtoFrom(e)));
        return dtos;
    }
}