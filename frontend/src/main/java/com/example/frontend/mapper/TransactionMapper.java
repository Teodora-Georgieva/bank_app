package com.example.frontend.mapper;

import com.example.frontend.dto.DisplayTransactionDto;
import com.example.frontend.dto.TransactionRespDto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TransactionMapper {
    private static final String DATE_FORMATTER = "dd-MM-yyyy HH:mm:ss";

    public static DisplayTransactionDto displayTransactionDtoFrom(TransactionRespDto response) {
        DisplayTransactionDto displayTransactionDto = new DisplayTransactionDto();
        LocalDateTime createdDate = response.getCreatedDate();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMATTER);
        String formatDateTime = createdDate.format(formatter);
        displayTransactionDto.setCreatedDate(formatDateTime);
        displayTransactionDto.setTransactionType(response.getTransactionType());
        displayTransactionDto.setBankAccount(response.getBankAccount());
        displayTransactionDto.setTransactionAmount(response.getTransactionAmount());

        return displayTransactionDto;
    }
}