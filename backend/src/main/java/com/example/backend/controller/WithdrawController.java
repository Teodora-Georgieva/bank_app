package com.example.backend.controller;

import com.example.backend.constants.Endpoints;
import com.example.backend.dto.TransactionRespDto;
import com.example.backend.dto.WithdrawRequestDto;
import com.example.backend.service.WithdrawService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class WithdrawController {
    @Autowired
    private WithdrawService withdrawService;

    @PostMapping(Endpoints.WITHDRAW)
    public ResponseEntity<?> withdraw(@Valid @RequestBody WithdrawRequestDto requestDto) {
        TransactionRespDto transactionRespDto = withdrawService.withdraw(requestDto);
        return ResponseEntity.ok(transactionRespDto);
    }
}