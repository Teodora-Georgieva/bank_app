package com.example.backend.service;

import com.example.backend.dto.RegistrationReqDto;
import com.example.backend.dto.RegistrationRespDto;
import com.example.backend.entity.BankAccountEntity;
import com.example.backend.entity.UserEntity;
import com.example.backend.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserRegisterServiceImpl implements UserRegisterService {
    @Autowired
    private UserService userService;
    @Autowired
    private BankAccountService bankAccountService;

    @Transactional
    public RegistrationRespDto register(RegistrationReqDto requestDto) {
        UserEntity userEntity = userService.saveUser(requestDto);
        BankAccountEntity bankAccountEntity = bankAccountService.saveBankAccount(requestDto, userEntity);
        userEntity.addBankAccount(bankAccountEntity);
        RegistrationRespDto respDto = UserMapper.userRegistrationRespDtoFrom(userEntity);
        return respDto;
    }
}