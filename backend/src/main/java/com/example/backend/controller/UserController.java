package com.example.backend.controller;

import com.example.backend.constants.Endpoints;
import com.example.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping(Endpoints.GET_NUMBER_OF_ONLINE_USERS)
    public ResponseEntity<?> getNumberOfOnlineUsers() {
        Integer numberOfOnlineUsers = userService.getNumberOfOnlineUsers();
        return ResponseEntity.ok(numberOfOnlineUsers);
    }
}