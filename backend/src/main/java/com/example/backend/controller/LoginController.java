package com.example.backend.controller;

import com.example.backend.constants.Endpoints;
import com.example.backend.dto.LoginReqDto;
import com.example.backend.dto.LoginRespDto;
import com.example.backend.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class LoginController {
    @Autowired
    private SessionService sessionService;

    @PutMapping(Endpoints.LOGIN)
    public ResponseEntity<?> login(@RequestBody LoginReqDto reqDto) {
        Optional<LoginRespDto> userLoginRespDto = sessionService.createSession(reqDto);
        if (userLoginRespDto.isPresent()) {
            return ResponseEntity.ok(userLoginRespDto);
        }

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }
}