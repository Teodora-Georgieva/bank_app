package com.example.backend.repository;

import com.example.backend.entity.UserSessionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SessionRepository extends JpaRepository<UserSessionEntity, Long> {
    void deleteAllByUserId(Long userId);

    void deleteByUserId(Long userId);

    Optional<UserSessionEntity> findBySessionId(String sessionId);
}