package com.example.backend.service;

import com.example.backend.currentuser.CurrentUser;
import com.example.backend.dto.Transactions;
import com.example.backend.entity.TransactionEntity;
import com.example.backend.mapper.TransactionMapper;
import com.example.backend.repository.TransactionRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@AllArgsConstructor
public class TransactionServiceImpl implements TransactionService {
    private final TransactionRepository transactionRepository;
    private final CurrentUser currentUser;

    public Transactions getLastFiveTransactions() {
        List<TransactionEntity> transactions = transactionRepository.findTop5ByUserIdOrderByCreatedDateDesc(currentUser.getId());
        Transactions userTransactions = new Transactions();
        userTransactions.addAll(TransactionMapper.transactionDtosFrom(transactions));
        return userTransactions;
    }

    public Transactions getAllTransactionsBetween(LocalDate startDate, LocalDate endDate) {
        List<TransactionEntity> transactions =
                transactionRepository.getAllTransactionsForUserAndPeriod(currentUser.getId(), startDate, endDate);
        Transactions userTransactions = new Transactions();
        userTransactions.addAll(TransactionMapper.transactionDtosFrom(transactions));
        return userTransactions;
    }
}