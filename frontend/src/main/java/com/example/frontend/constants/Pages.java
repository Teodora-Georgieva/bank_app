package com.example.frontend.constants;

public class Pages {
    public static final String PAGES_ROOT = "/page";
    public static final String REGISTER = PAGES_ROOT + "/register.xhtml";
    public static final String LOGIN = PAGES_ROOT + "/login.xhtml";
    public static final String HOMEPAGE = PAGES_ROOT + "/homepage.xhtml";
    public static final String DEPOSIT = PAGES_ROOT + "/deposit.xhtml";
    public static final String WITHDRAW = PAGES_ROOT + "/withdraw.xhtml";
    public static final String REPORT = PAGES_ROOT + "/report.xhtml";
}