package com.example.frontend.mapper;

import com.example.frontend.dto.BankAccountRespDto;
import com.example.frontend.dto.HomepageBankAccountDto;

public class BankAccountMapper {
    public static HomepageBankAccountDto homepageBankAccountFrom(BankAccountRespDto response) {
        Integer bankNumber = response.getBankNumber();
        Integer bankBranchNumber = response.getBankBranchNumber();
        Integer bankAccountNumber = response.getBankAccountNumber();
        String bankAccount = bankNumber + "-" + bankBranchNumber + "-" + bankAccountNumber;
        HomepageBankAccountDto homepageBankAccount = new HomepageBankAccountDto();
        homepageBankAccount.setBankAccount(bankAccount);
        homepageBankAccount.setCurrentBalance(response.getCurrentBalance());
        return homepageBankAccount;
    }
}