package com.example.backend.repository;

import com.example.backend.entity.TransactionEntity;
import lombok.AllArgsConstructor;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
public class CustomTransactionRepositoryImpl implements CustomTransactionRepository {
    private final EntityManager entityManager;

    public List<TransactionEntity> getAllTransactionsForUserAndPeriod(Long userId, LocalDate startDate, LocalDate endDate) {
        String query = "SELECT * FROM transactions " +
                "WHERE user_id=:userId " +
                "AND DATE(created_date) BETWEEN :start AND :end " +
                "ORDER BY created_date DESC";

        List transactions = entityManager.createNativeQuery(query, TransactionEntity.class)
                .setParameter("userId", userId)
                .setParameter("start", startDate)
                .setParameter("end", endDate)
                .getResultList();

        return transactions;
    }
}