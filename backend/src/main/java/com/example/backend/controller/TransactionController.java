package com.example.backend.controller;

import com.example.backend.constants.Endpoints;
import com.example.backend.dto.Transactions;
import com.example.backend.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
public class TransactionController {
    @Autowired
    private TransactionService transactionService;

    @GetMapping(Endpoints.GET_LAST_FIVE_TRANSACTIONS)
    public ResponseEntity<?> getLastFiveTransactions() {
        Transactions userTransactions = transactionService.getLastFiveTransactions();
        return ResponseEntity.ok(userTransactions);
    }

    @GetMapping(Endpoints.GET_ALL_TRANSACTIONS_BETWEEN)
    public ResponseEntity<?> getAllTransactionsBetween(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
                                                       @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate) {
        Transactions userTransactions = transactionService.getAllTransactionsBetween(startDate, endDate);
        return ResponseEntity.ok(userTransactions);
    }
}