package com.example.backend.repository;

import com.example.backend.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    UserEntity findByEmail(String email);

    UserEntity findByUsername(String username);

    Optional<UserEntity> findByUsernameAndPassword(String username, String password);

    @Query("SELECT e FROM UserEntity e JOIN UserSessionEntity se ON e.id = se.userId " +
            "WHERE se.sessionId=:sid")
    UserEntity findBySid(@Param("sid") String sid);

    @Query(value = "SELECT COUNT(*) FROM user_session s WHERE s.expires_on > NOW()", nativeQuery = true)
    int getNumberOfOnlineUsers();
}