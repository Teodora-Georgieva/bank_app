package com.example.frontend.beans;

import com.example.frontend.clients.UserClient;
import com.example.frontend.dto.LoginReqDto;
import com.example.frontend.dto.LoginRespDto;
import com.example.frontend.http.HttpResponse;
import com.example.frontend.utils.Router;
import com.example.frontend.views.MessageView;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import java.io.IOException;

@ManagedBean
@Data
@ViewScoped
public class LoginBean {
    private LoginReqDto userLoginReqDto;
    @Autowired
    private UserClient userClient;
    @Autowired
    private Router router;
    @Autowired
    private CurrentUserBean currentUserBean;
    @Autowired
    private RestTemplate restTemplate;

    @PostConstruct
    private void init() {
        this.userLoginReqDto = new LoginReqDto();
    }

    public void login() throws IOException {
        HttpResponse response = userClient.loginUser(userLoginReqDto);

        if (response.isOK()) {
            LoginRespDto body = response.read(LoginRespDto.class);
            currentUserBean.setUserLoginRespDto(body);
            router.goToHomepage();
        } else {
            MessageView.addWarningMessage("Wrong username or password!", null);
        }
    }

    public void logout() throws IOException {
        userClient.logoutUser();
        currentUserBean.setUserLoginRespDto(null);
        Router.goToLogin();
    }
}