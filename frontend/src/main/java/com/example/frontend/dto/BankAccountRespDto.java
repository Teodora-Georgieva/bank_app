package com.example.frontend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BankAccountRespDto {
    private Long id;
    private Integer bankNumber;
    private Integer bankBranchNumber;
    private Integer bankAccountNumber;
    private BigDecimal currentBalance;
}