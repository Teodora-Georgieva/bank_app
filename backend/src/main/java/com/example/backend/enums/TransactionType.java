package com.example.backend.enums;

public enum TransactionType {
    DEPOSIT, WITHDRAWAL
}