package com.example.backend.repository;

import com.example.backend.entity.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionEntity, Long>, CustomTransactionRepository {
    List<TransactionEntity> findTop5ByUserIdOrderByCreatedDateDesc(Long userId);
}