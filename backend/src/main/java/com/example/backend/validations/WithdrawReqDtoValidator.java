package com.example.backend.validations;

import com.example.backend.dto.WithdrawRequestDto;
import com.example.backend.service.WithdrawService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

public class WithdrawReqDtoValidator implements
        ConstraintValidator<ValidWithdrawRequestDto, WithdrawRequestDto> {

    @Autowired
    private WithdrawService withdrawService;

    @Override
    public void initialize(ValidWithdrawRequestDto constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(WithdrawRequestDto requestDto, ConstraintValidatorContext context) {
        Long bankAccountId = requestDto.getBankAccountId();
        BigDecimal crrBalance = withdrawService.findCurrentBalanceByBankAccountId(bankAccountId);
        BigDecimal withdrawAmount = requestDto.getWithdrawAmount();
        if (withdrawAmount.compareTo(BigDecimal.ONE) < 0 || crrBalance.compareTo(withdrawAmount) < 0) {
            context.
                    buildConstraintViolationWithTemplate("cannot withdraw - current balance is less than withdraw amount or withdraw amount is less than 1").
                    addNode("withdrawAmount").addConstraintViolation();
            return false;
        }
        return true;
    }
}