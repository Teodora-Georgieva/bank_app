package com.example.backend.currentuser;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderMethodName = "aNewCurrentUser")
public class CurrentUser {
    private Long id;
    private String firstName;
    private String lastName;
    private String username;
    private String email;
}