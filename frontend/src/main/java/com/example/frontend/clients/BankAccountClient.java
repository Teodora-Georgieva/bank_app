package com.example.frontend.clients;

import com.example.frontend.constants.Endpoints;
import com.example.frontend.dto.DepositRequestDto;
import com.example.frontend.dto.WithdrawRequestDto;
import com.example.frontend.http.HttpRequest;
import com.example.frontend.http.HttpResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Slf4j
@Component
@AllArgsConstructor
public class BankAccountClient {
    private final HttpRequest httpRequest;

    public HttpResponse getTotalBalance() {
        return httpRequest.get(Endpoints.GET_TOTAL_BALANCE);
    }

    public HttpResponse deposit(Long bankAccountId, BigDecimal depositAmount) {
        DepositRequestDto depositRequestDto = new DepositRequestDto(bankAccountId, depositAmount);
        return httpRequest.post(Endpoints.DEPOSIT, depositRequestDto);
    }

    public HttpResponse getAllBankAccounts() {
        return httpRequest.get(Endpoints.GET_ALL_BANK_ACCOUNTS);
    }

    public HttpResponse withdraw(Long bankAccountId, BigDecimal withdrawAmount) {
        WithdrawRequestDto withdrawRequestDto = new WithdrawRequestDto(bankAccountId, withdrawAmount);
        return httpRequest.post(Endpoints.WITHDRAW, withdrawRequestDto);
    }
}