package com.example.backend.service;

import com.example.backend.dto.LoginReqDto;
import com.example.backend.dto.LoginRespDto;

import java.util.Optional;

public interface SessionService {
    Optional<LoginRespDto> createSession(LoginReqDto reqDto);

    void logout();

    boolean isValidSid(String sid);

    void refresh(String sid);
}