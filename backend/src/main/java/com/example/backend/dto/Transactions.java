package com.example.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Transactions {
    private List<TransactionRespDto> userTransactions = new ArrayList<>();

    public void add(TransactionRespDto transactionRespDto) {
        userTransactions.add(transactionRespDto);
    }

    public void addAll(List<TransactionRespDto> transactionDtos) {
        userTransactions.addAll(transactionDtos);
    }
}