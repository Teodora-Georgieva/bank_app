package com.example.frontend.beans;

import com.example.frontend.clients.UserClient;
import com.example.frontend.dto.RegistrationReqDto;
import com.example.frontend.dto.RegistrationRespDto;
import com.example.frontend.enums.Gender;
import com.example.frontend.http.HttpResponse;
import com.example.frontend.utils.Router;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import java.io.IOException;

@Slf4j
@Data
@ManagedBean
@ViewScoped
public class RegisterBean {
    private RegistrationReqDto userReqDto;
    @Autowired
    private Router router;
    @Autowired
    private UserClient userClient;
    @Autowired
    private CurrentUserBean currentUserBean;

    @PostConstruct
    private void init() {
        userReqDto = new RegistrationReqDto();
    }

    public Gender[] getGenderValues() {
        return Gender.values();
    }

    public void register() throws IOException {
        HttpResponse response = userClient.registerUser(userReqDto);
        String message;
        if (response.isOK()) {
            response.read(RegistrationRespDto.class);
            currentUserBean.setUserLoginRespDto(null);
            router.goToLogin();
        } else {
            message = "Unsuccessful registration - wrong credentials!";
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, message, "");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }
}