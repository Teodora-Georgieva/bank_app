package com.example.backend.entity;

import com.example.backend.enums.TransactionType;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "transactions")
public class TransactionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity user;
    @Column(name = "amount")
    private BigDecimal depositOrWithdrawAmount;
    @CreationTimestamp
    private LocalDateTime createdDate;
    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private TransactionType transactionType;
    @ManyToOne
    @JoinColumn(name = "bank_account_id", nullable = false)
    private BankAccountEntity bankAccountEntity;

    public Long getId() {
        return id;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public BigDecimal getDepositOrWithdrawAmount() {
        return depositOrWithdrawAmount;
    }

    public void setDepositOrWithdrawAmount(BigDecimal depositOrWithdrawAmount) {
        this.depositOrWithdrawAmount = depositOrWithdrawAmount;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public void setBankAccountEntity(BankAccountEntity bankAccountEntity) {
        this.bankAccountEntity = bankAccountEntity;
    }

    public BankAccountEntity getBankAccountEntity() {
        return bankAccountEntity;
    }
}