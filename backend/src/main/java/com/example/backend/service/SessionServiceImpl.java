package com.example.backend.service;

import com.example.backend.configuration.ConfigProperties;
import com.example.backend.currentuser.CurrentUser;
import com.example.backend.dto.LoginReqDto;
import com.example.backend.dto.LoginRespDto;
import com.example.backend.entity.UserEntity;
import com.example.backend.entity.UserSessionEntity;
import com.example.backend.mapper.UserMapper;
import com.example.backend.repository.SessionRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class SessionServiceImpl implements SessionService {
    private final UserService userService;
    private final ConfigProperties configProperties;
    private final SessionRepository sessionRepository;
    private final CurrentUser currentUser;

    @Transactional
    public Optional<LoginRespDto> createSession(LoginReqDto reqDto) {
        Optional<UserEntity> userEntityOptional =
                userService.findByUsernameAndPassword(reqDto.getUsername(), reqDto.getPassword());

        if (!userEntityOptional.isPresent()) {
            return Optional.empty();
        }

        UserEntity user = userEntityOptional.get();
        sessionRepository.deleteAllByUserId(user.getId());

        UUID sessionId = UUID.randomUUID();
        long sessionTTL = configProperties.getSessionTimeToLive();
        UserSessionEntity userSessionEntity = new UserSessionEntity();
        userSessionEntity.setUserId(user.getId());
        userSessionEntity.setSessionId(sessionId.toString());
        userSessionEntity.setExpiresOn(LocalDateTime.now().plusMinutes(sessionTTL));
        userSessionEntity = sessionRepository.save(userSessionEntity);

        LoginRespDto respDto = UserMapper.userLoginRespDtoFrom(user);
        respDto.setSessionId(userSessionEntity.getSessionId());
        return Optional.of(respDto);
    }

    @Transactional
    public void logout() {
        sessionRepository.deleteByUserId(currentUser.getId());
    }

    public boolean isValidSid(String sid) {
        Optional<UserSessionEntity> sessionOptional = sessionRepository.findBySessionId(sid);
        if (!sessionOptional.isPresent()) {
            return false;
        }

        UserSessionEntity session = sessionOptional.get();
        LocalDateTime expiresOn = session.getExpiresOn();
        if (expiresOn.isBefore(LocalDateTime.now())) {
            return false;
        }

        return true;
    }

    public void refresh(String sid) {
        UserSessionEntity session = sessionRepository.findBySessionId(sid).get();
        LocalDateTime expiresOn = LocalDateTime.now();
        long sessionTTL = configProperties.getSessionTimeToLive();
        expiresOn = expiresOn.plus(sessionTTL, ChronoUnit.MINUTES);
        session.setExpiresOn(expiresOn);
        sessionRepository.save(session);
    }
}