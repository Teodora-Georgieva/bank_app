package com.example.frontend.webfilters;

import com.example.frontend.beans.CurrentUserBean;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@AllArgsConstructor
@Component
@Slf4j
public class SecurityFilter implements Filter {
    private final CurrentUserBean currentUserBean;

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        String requestURI = req.getRequestURI();
        log.debug("Request to {}", requestURI);

        if ("/page/login.xhtml".equals(requestURI) && currentUserBean.getUserLoginRespDto() != null) {
            resp.sendRedirect("/page/homepage.xhtml");
            chain.doFilter(request, response);
            return;
        }

        if (requestURI.contains("login") || requestURI.contains("register") || requestURI.contains("css")
                || requestURI.contains("javax") || requestURI.contains("favicon")) {
            chain.doFilter(request, response);
            log.debug("====");
            return;
        }

        if (currentUserBean.getUserLoginRespDto() == null) {
            resp.sendRedirect("/page/login.xhtml");
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}