package com.example.backend.validations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DepositReqDtoValidator.class)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidDepositRequestDto {
    String message() default "Invalid deposit request";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}