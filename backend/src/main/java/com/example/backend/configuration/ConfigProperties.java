package com.example.backend.configuration;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Data
public class ConfigProperties {
    @Value("${session.time_to_live}")
    private long sessionTimeToLive;
}